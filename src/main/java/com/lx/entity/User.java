package com.lx.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * @author 段誉
 * @create 2019-04-11 9:48
 */
@Data
public class User implements Serializable {
  private Integer userId;
  private String username;
}
