package com.lx.dao;

import com.lx.entity.User;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author 段誉
 * @create 2019-04-11 9:49
 */
@Mapper
public interface UserDao {
  /**
   * 添加用户
   * @param user
   * @return
   */
  @Insert("insert into user(user_id, username) value(#{userId}, #{username})")
  int userInsert(User user);

  /**
   * 根据用户id查询
   * @param userId
   * @return
   */
  @Select("select user_id as userId, username from user where user_id=#{userId}")
  User userQuery(Integer userId);
}
