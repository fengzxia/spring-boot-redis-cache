package com.lx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * 项目启动类
 *
 * @author 段誉
 * @create 2019-04-11 9:32
 */
@SpringBootApplication
//开启缓存
@EnableCaching
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class);
  }
}
