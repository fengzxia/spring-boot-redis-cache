package com.lx.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;

/**
 * redis配置
 *
 * @author 段誉
 * @create 2019-04-11 11:01
 */
@Configuration
public class RedisConfig {
  /**
   * @desc 序列化缓存定制
   * @Author 段誉
   * @Date 2019/4/11 13:01
   * @method
   */
  @Bean
  public RedisCacheManager jsonCacheManager(RedisConnectionFactory factory) {
    RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig()
            .serializeValuesWith(RedisSerializationContext.SerializationPair
                    //使用GenericJackson2JsonRedisSerializer可以同时支持多种不同类型的domain对象
                .fromSerializer(new GenericJackson2JsonRedisSerializer()));
    return RedisCacheManager.builder(factory)
            .cacheDefaults(config)
            .build();
  }
}
