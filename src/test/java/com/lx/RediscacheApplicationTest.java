package com.lx;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * redis连接测试
 *
 * @author 段誉
 * @create 2019-04-11 9:27
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RediscacheApplicationTest {
  @Autowired
  private StringRedisTemplate stringRedisTemplate;

  @Test
  public void contextLoads() {
    stringRedisTemplate.opsForValue().set("k1", "v1");
    System.out.println(stringRedisTemplate.opsForValue().get("k1"));
  }
}
