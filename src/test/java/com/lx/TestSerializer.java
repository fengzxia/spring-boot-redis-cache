package com.lx;

import com.alibaba.fastjson.JSON;
import com.lx.entity.User;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 测试序列化
 *
 * @author 段誉
 * @create 2019-04-11 13:45
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestSerializer {
  @Autowired
  private RedisTemplate redisTemplate;

  @Test
  public void jackson2JsonRedisSerializer() {
    redisTemplate.setKeySerializer(new StringRedisSerializer());
    redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer(Object.class));

    long start = System.currentTimeMillis();
    User user = new User();
    user.setUserId(1);
    user.setUsername("张三");
    List<User> userList = new ArrayList<>();
    for (int i = 0; i < 10000; i++) {
      userList.add(user);
    }
    //不能直接将对象存储进redis中否则在进行反序列化的时候会报
    // java.lang.ClassCastException: java.util.LinkedHashMap cannot be cast to com.lx.entity.User错误
    //可以先转为json字符串再进行存储
    redisTemplate.opsForValue().set("jackson2JsonRedisSerializer", JSON.toJSONString(userList), 5,
            TimeUnit.MINUTES);

    long end = System.currentTimeMillis();
    System.out.println("jackson2JsonRedisSerializer序列化需要的时间:" + (end - start));
  }

  @Test
  public void genericJackson2JsonRedisSerializer() {
    redisTemplate.setKeySerializer(new StringRedisSerializer());
    redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());

    long start = System.currentTimeMillis();
    User user = new User();
    user.setUserId(1);
    user.setUsername("李四");
    List<User> userList = new ArrayList<>();
    for (int i = 0; i < 10000; i++) {
      userList.add(user);
    }
    redisTemplate.opsForValue().set("genericJackson2JsonRedisSerializer", JSON.toJSONString(userList),5,TimeUnit.MINUTES);
    long end = System.currentTimeMillis();
    System.out.println("genericJackson2JsonRedisSerializer序列化需要的时间:" + (end - start));
  }

}
